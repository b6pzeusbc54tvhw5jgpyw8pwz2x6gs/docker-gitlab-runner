# docker-gitlab-runner
Usage:

### 1. run gitlab-runner process

```
$ ./run.sh <your_runner_name>
``` 
then, config directory named "your_runner_name" will be made in the current directory.


### 2. register

```
register.sh <your_runner_name>
```

if you done register successfully, in your config directory, `config.toml` file will be created.

### 3. setting
If you want to add some your setting, edit this `config.toml` file,

```
$ vim <your_runner_name>/config.toml
```

example:

```toml
concurrent = 6
check_interval = 0

[[runners]]
  name = "<your_runner_name"
  url = "https://your.gitlab.domain/ci"
  token = "c5a571221a05xxxxxxxx9070be5078"
  executor = "docker"
  [runners.docker]
    tls_verify = false
    image = "node:4.4.7"
    privileged = false
    disable_cache = false
    volumes = ["/cache"]
    extra_hosts = ["some.your.domain:123.123.123.123"]
  [runners.cache]
    Type = "s3"
    ServerAddress = "s3.amazonaws.com"
    AccessKey = "AMAZON_S3_ACCESS_KEY"
    SecretKey = "AMAZON_S3_SECRET_KEY"
    BucketName = "runners"
    BucketLocation = "eu-west-1"
    Insecure = false
```

Please refer the official manual https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/blob/master/docs/configuration/advanced-configuration.md

### 4. restart

After restart above <your_runner_name> container, new registered runner and new config will apply.

```
$ ./run.sh <your_runner_name>
``` 
