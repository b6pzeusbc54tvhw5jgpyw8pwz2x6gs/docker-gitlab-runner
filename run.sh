mkdir $1
docker stop $1
docker rm $1
docker run -d --name $1 --restart always \
  --volume /var/run/docker.sock:/var/run/docker.sock \
  --volume $PWD/$1:/etc/gitlab-runner \
  gitlab/gitlab-runner:v1.5.3
